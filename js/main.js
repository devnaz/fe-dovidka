$(function(){

//    $(window).scroll(function() {
//        var scrlTop = $(this).scrollTop();
//        var yPos = ($(window).scrollTop() / 5);
//        
//        var banner = $(".main-header");
//        banner.css("background-position" , -yPos + "px " + "0px" );
//    });


    $( "#accordion" ).accordion({
        collapsible: true,
        animate: 200,
    });
    
    
    $(".owl-carousel").owlCarousel({
        items : 4,
        
//        itemsDesktop: [1440,3],
        
        navigation : true,
        pagination: true,
        
        afterAction: function () {
            var currentItem = this.owl.currentItem;
            var owlItems = this.owl.owlItems.length
            var visibleItems = this.owl.visibleItems.length;
            
            if (currentItem == 0) {
                $(".owl-prev").hide();
            }
            else {
                $(".owl-prev").show();
            }
            
            if (currentItem == (owlItems - visibleItems)) {
                $(".owl-next").hide();
            }
            else {
                $(".owl-next").show();
            }
        }
    });
    
    
    $( "#tabs" ).tabs();
    
    
    
    
//    function initialize() {
//        var mapProp = {
//            center:new google.maps.LatLng(51.508742,-0.120850),
//            zoom:11,
//            mapTypeId:google.maps.MapTypeId.ROADMAP
//        };
//        var map=new google.maps.Map(document.getElementById("map"),mapProp);
//    }
//    google.maps.event.addDomListener(window, 'load', initialize);



});